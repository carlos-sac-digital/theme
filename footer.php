<?php
/**
 * The template for displaying the footer.
 *
 * Please browse readme.txt for credits and forking information
 * Contains the closing of the #content div and all content after
 *
 * @package writers
 */

?>

</div><!-- #content -->

<?php if ( is_active_sidebar( 'footer_widget_left') ||  is_active_sidebar( 'footer_widget_middle') ||  is_active_sidebar( 'footer_widget_right')  ) : ?>
<div class="footer-widget-wrapper">
	<div class="container">

		<div class="row">
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer_widget_left' ); ?>
			</div>
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer_widget_middle' ); ?>
			</div>
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer_widget_right' ); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<footer id="colophon" class="site-footer">
	<div class="row" style="margin-bottom:0px;">
		<div class="col-md-1 footer-pad"></div>
		<div class="col-md-5 footer1">
			<div class="row">
				<div class="col-md-4">
					<div class="r-box">
							<p>Comercial</p>
							<ul>
									<li><a href="tel:+5527981843007" onclick="ga('telefoneComercialFooter.send', 'event', 'telefoneComercialFooter', 'click', 'Footer')"><i class="fa fa-phone"></i> ( 27 ) 98184 - 3007</a></li>
									<li><a href="mailto:atendimento@sac.digital" onclick="ga('emailComercialFooter.send', 'event', 'emailComercialFooter', 'click', 'Footer')"><i class="fa fa-envelope"></i> atendimento@sac.digital</a></li>
							</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="r-box">
							<p>Suporte Técnico</p>
							<ul>
									<li><a href="tel:+552732110011" onclick="ga('telefoneSuporteFooter.send', 'event', 'telefoneSuporteFooter', 'click', 'Footer')"><i class="fa fa-phone"></i> ( 27 ) 3211 - 0011</a></li>
									<li><a href="mailto:suporte@sac.digital" onclick="ga('emailSuporteFooter.send', 'event', 'emailSuporteFooter', 'click', 'Footer')"><i class="fa fa-envelope"></i> suporte@sac.digital</a></li>
							</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="r-box">
						 <p>Financeiro</p>
						 <ul>
								 <li><a href="tel:+5527981842200" onclick="ga('telefoneFinancieiroFooter.send', 'event', 'telefoneFinancieiroFooter', 'click', 'Footer')"><i class="fa fa-phone"></i> ( 27 ) 98184 - 2200</a></li>
								 <li><a href="mailto:financeiro@sac.digital" onclick="ga('emailFinancieiroFooter.send', 'event', 'emailFinancieiroFooter', 'click', 'Footer')"><i class="fa fa-envelope"></i> financeiro@sac.digital</a></li>
						 </ul>
				 </div>
				</div>
			</div>
		</div>
		<div class="col-md-5 footer2">
			<div class="row" style="margin-bottom:0px;">
				<div class="col-md-12">
					<div class="menu-rodape hide_xs hide_xmd">
					   <!--<p>Navegação</p>-->
					   <ul>
							 <li><p style="text-decoration:underline;">Navegação</p></li>
					     <li><a class="scroll" href="#home" onclick="ga('inicioMenuFooter.send', 'event', 'inicioMenuFooter', 'click', 'Footer')">Inicio</a></li>
					     <li><a class="scroll" href="#plataforma" onclick="ga('plataformaMenuFooter.send', 'event', 'plataformaMenuFooter', 'click', 'Footer')">Plataforma</a></li>
					     <li><a class="scroll" href="#vantagens" onclick="ga('vantagensMenuFooter.send', 'event', 'vantagensMenuFooter', 'click', 'Footer')">Vantagens</a></li>
					     <li><a class="scroll" href="#tecnologias" onclick="ga('tecnologiasMenuFooter.send', 'event', 'tecnologiasMenuFooter', 'click', 'Footer')">Tecnologias</a></li>
					     <li><a class="scroll" href="#contato" onclick="ga('contatoMenuFooter.send', 'event', 'contatoMenuFooter', 'click', 'Footer')">Contato</a></li>
					     <li><a href="#" onclick="ga('blogMenuFooter.send', 'event', 'blogMenuFooter', 'click', 'Footer')">Blog</a></li>
					     <li><a href="#" onclick="ga('FAQMenuFooter.send', 'event', 'FAQMenuFooter', 'click', 'Footer')">FAQ</a></li>
					   </ul>
					</div>
				</div>
				<div class="col-md-12">
					<div class="social-rodape">
					 	<p>Conecte-se</p>
					     <ul>
					       <li class="li-facebook"><a href="https://www.facebook.com/sacdigitalbr/" title="Facebook" target="_blank" onclick="ga('facebookFooter.send', 'event', 'facebookFooter', 'click', 'Footer')"><i class="fa fa-facebook"></i></a></li>
					       <li class="li-twitter"><a href="https://twitter.com/sacdigitalbr" title="Twitter" target="_blank" onclick="ga('twitterFooter.send', 'event', 'twitterFooter', 'click', 'Footer')"><i class="fa fa-twitter"></i></a></li>
					       <li class="li-youtube"><a href="https://www.youtube.com/channel/UCXSjILPCsSzJi0DkfJEtqwQ" title="Youtube" target="_blank" onclick="ga('youtubeFooter.send', 'event', 'youtubeFooter', 'click', 'Footer')"><i class="fa fa-youtube"></i></a></li>
					       <li class="li-google-plus"><a href="https://plus.google.com/+sacdigitalbr" title="Google+" target="_blank" onclick="ga('googleFooter.send', 'event', 'googleFooter', 'click', 'Footer')"><i class="fa fa-google-plus"></i></a></li>
					       <li class="li-android"><a href="https://play.google.com/store/apps/dev?id=6360780013750049703" title="Alertrack Soluções" target="_blank" onclick="ga('alertrackAppFooter.send', 'event', 'alertrackAppFooter', 'click', 'Footer')"><i class="fa fa-android"></i></a></li>
					       <li class="li-whatsapp hidden-md"><a href="whatsapp://send?text=Oi&amp;phone=+5527981860090" title="Whatsapp" target="_blank" onclick="ga('whatsappFooter.send', 'event', 'whatsappFooter', 'click', 'Footer')"><i class="fa fa-whatsapp"></i></a></li>
					       <li class="li-telegram"><a href="tg://resolve?domain=sacdigitalbr" target="_blank" title="Telegram" onclick="ga('telegramFooter.send', 'event', 'telegramFooter', 'click', 'Footer')"><i class="fa fa-telegram"></i></a></li>
					    </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-1 footer-pad"></div>
	</div>

	<div class="row site-info">
		<div class="text-center">® 2012 - 2017 Alertrack. Todos os direitos reservados. </div>
	</div><!-- .site-info -->

</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
