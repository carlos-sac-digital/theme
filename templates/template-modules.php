<?php
/**
 * Template Name: Modules
 * Full width template for writers theme
 *
 * Please browse readme.txt for credits and forking information
 * @package writers
 */

get_header('modulo'); ?>

      <div class="row">
				<div id="primary" class="col-md-12 content-area">
					<main id="main" class="site-main" role="main">
            <span class="title"><h1>MÓDULOS</h1></span>

            <div class="row">
              <div class="col-md-6">
                <img id="tablet" src="http://localhost/sacdigital/wp-content/uploads/2017/11/tablet.png">
              </div>
              <div class="col-md-6">
                <div id="title">
                <h2>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI,
                  quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos.</h2>
                </div>
              </div>
            </div>

            <div class="row">
              <div id="modules-title">
                <h2>CONHECA CADA MODULO</h2>
                <hr>
                <h4>A seguir, confira os módulos de nossa plataforma</h4>
              </div>
              <div id="modules-list">
                <div class="modules-item item-left">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>GERENCIAMENTO DE PROTOCOLOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-right">
                  <div class="row">
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>IDENTIFICACAO SOCIAL DE CONTATOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-left">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>GERENCIAMENTO DE PROTOCOLOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-right">
                  <div class="row">
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>IDENTIFICACAO SOCIAL DE CONTATOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-left">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>GERENCIAMENTO DE PROTOCOLOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-right">
                  <div class="row">
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>IDENTIFICACAO SOCIAL DE CONTATOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-left">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>GERENCIAMENTO DE PROTOCOLOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modules-item item-right">
                  <div class="row">
                    <div class="col-md-9">
                      <div class="modules-description">
                        <h3>IDENTIFICACAO SOCIAL DE CONTATOS</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non quam sollicitudin, maximus nunc sed, lobortis orci. Curabitur feugiat commodo mauris, sed tincidunt leo maximus ac. Donec gravida lacus non leo aliquet, pretium tincidunt orci semper. Proin a lectus quis lectus egestas porttitor. Cras volutpat velit ac elit blandit vehicula. Fusce ligula magna, congue in massa ut, finibus fringilla quam. Integer commodo id justo id vulputate. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="module-picture"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

					</main><!-- #main -->
				</div><!-- #primary -->

			</div> <!--.row-->
        <?php get_footer(); ?>
