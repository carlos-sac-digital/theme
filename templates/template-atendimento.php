<?php
/**
 * Template Name: Atendimento
 * Full width template for writers theme
 *
 * Please browse readme.txt for credits and forking information
 * @package writers
 */

get_header('modulo'); ?>

      <div class="row">
				<div id="primary" class="col-md-12 content-area">
					<main id="main" class="site-main" role="main">
						<span class="title"><h1>ATENDIMENTO OPERACIONAL</h1></span>
							<div class="row">
								<div class="col-md-6 mac"><img src="http://localhost/sacdigital/wp-content/uploads/2017/12/Tela-Mac.png"></div>
								<div class="col-md-6 items">
									<div class="row item">
										<div class="col-md-2"></div>
										<div class="col-md-1"><img src="http://localhost/sacdigital/wp-content/uploads/2017/12/Icone-1.png"></div>
										<div class="col-md-9"><h3>Gerenciamento de protocolos</h3></div>
									</div>
									<div class="row item">
										<div class="col-md-2"></div>
										<div class="col-md-1"><img src="http://localhost/sacdigital/wp-content/uploads/2017/12/Icone-2.png"></div>
										<div class="col-md-9"><h3>Identificação social de contatos</h3></div>
									</div>
									<div class="row item">
										<div class="col-md-2"></div>
										<div class="col-md-1"><img src="http://localhost/sacdigital/wp-content/uploads/2017/12/Icone-3.png"></div>
										<div class="col-md-9"><h3>Comunicação em tempo real</h3></div>
									</div>
									<div class="row item">
										<div class="col-md-2"></div>
										<div class="col-md-1"><img src="http://localhost/sacdigital/wp-content/uploads/2017/12/Icone-4.png"></div>
										<div class="col-md-9"><h3>Chat entre operadores</h3></div>
									</div>
									<div class="row tutorial-row">
										<div class="col-md-2"></div>
										<div class="col-md-7"><a href="#" id="tutorial">ASSISTIR TUTORIAL COMPLETO</a></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div id="mac">
									<img src="http://localhost/sacdigital/wp-content/uploads/2017/12/Tela-Mac.png">
								</div>
							</div>
							<div class="row">
								<div id="features_title">
									<h2>CARACTERÍSTICAS DA FERRAMENTA</h2>
									<hr>
									<h4>A seguir, confira as principais características do painel do operador.</h4>
								</div>

								<div class="row feature_item">
									<div class="col-md-5">
										<div class="picture"></div>
									</div>
									<div class="col-md-7 description">
										<h4>GERENCIAMENTO DE PROTOCOLOS</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin, dolor nec volutpat pellentesque, enim purus viverra felis, eget pretium nibh odio ac diam. Morbi vel magna in neque consectetur suscipit. Donec interdum id mi non vulputate. Donec rhoncus, enim sed commodo rhoncus, libero felis semper quam, nec sodales tortor metus vitae ante. Nam a commodo eros. Pellentesque non lorem ac risus convallis vestibulum at eu urna. Cras ultricies metus sit amet ullamcorper faucibus.</p>
									</div>
								</div>

								<div class="row feature_item feature-black">
									<div class="col-md-7 description">
										<h4>GERENCIAMENTO DE PROTOCOLOS</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin, dolor nec volutpat pellentesque, enim purus viverra felis, eget pretium nibh odio ac diam. Morbi vel magna in neque consectetur suscipit. Donec interdum id mi non vulputate. Donec rhoncus, enim sed commodo rhoncus, libero felis semper quam, nec sodales tortor metus vitae ante. Nam a commodo eros. Pellentesque non lorem ac risus convallis vestibulum at eu urna. Cras ultricies metus sit amet ullamcorper faucibus.</p>
									</div>
									<div class="col-md-5">
										<div class="picture"></div>
									</div>
								</div>

								<div class="row feature_item">
									<div class="col-md-5">
										<div class="picture"></div>
									</div>
									<div class="col-md-7 description">
										<h4>GERENCIAMENTO DE PROTOCOLOS</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin, dolor nec volutpat pellentesque, enim purus viverra felis, eget pretium nibh odio ac diam. Morbi vel magna in neque consectetur suscipit. Donec interdum id mi non vulputate. Donec rhoncus, enim sed commodo rhoncus, libero felis semper quam, nec sodales tortor metus vitae ante. Nam a commodo eros. Pellentesque non lorem ac risus convallis vestibulum at eu urna. Cras ultricies metus sit amet ullamcorper faucibus.</p>
									</div>
								</div>

								<div class="row feature_item feature-black">
									<div class="col-md-7 description">
										<h4>GERENCIAMENTO DE PROTOCOLOS</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin, dolor nec volutpat pellentesque, enim purus viverra felis, eget pretium nibh odio ac diam. Morbi vel magna in neque consectetur suscipit. Donec interdum id mi non vulputate. Donec rhoncus, enim sed commodo rhoncus, libero felis semper quam, nec sodales tortor metus vitae ante. Nam a commodo eros. Pellentesque non lorem ac risus convallis vestibulum at eu urna. Cras ultricies metus sit amet ullamcorper faucibus.</p>
									</div>
									<div class="col-md-5">
										<div class="picture"></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div id="clients-title">
										<h2>CASES DE NOSSOS CLIENTES</h2>
										<hr>
									</div>
								</div>
								<div class="col-md-12 clients">
									<div class="row">
										<div class="col-md-3"><div class="picture-client"></div><h4>Nome do Cliente</h4></div>
										<div class="col-md-3"><div class="picture-client"></div><h4>Nome do Cliente</h4></div>
										<div class="col-md-3"><div class="picture-client"></div><h4>Nome do Cliente</h4></div>
										<div class="col-md-3"><div class="picture-client"></div><h4>Nome do Cliente</h4></div>
									</div>
								</div>
							</div>
							<div class="row">
								<div id="suporte">
						        <div class="row container">
											<div class="col-md-6 suporte-text">
												<div class="s-left">
						                <div class="s-info">
						                    <h1>Suporte disponível para te atender</h1>
						                    <p>Queremos que você tenha a melhor experiência de atendimento, até quando estiver com problemas ou dúvidas!</p>
						                </div>
						                <div class="s-buttons">
						                    <a href="tel:+552732110011" class="phone" onclick="ga('telefoneContato.send', 'event', 'telefoneContato', 'click', 'Contato')"><i class="ti-mobile"></i> ( 27 ) 3211 - 0011</a>
																<a href="skype:sacdigitalbr?call" class="skype" onclick="ga('skypeContato.send', 'event', 'skypeContato', 'click', 'Contato')"><i class="fa fa-skype"></i> sacdigitalbr</a>
						                    <a href="mailto:atendimento@sac.digital" class="email" onclick="ga('emailContato.send', 'event', 'emailContato', 'click', 'Contato')"><i class="ti-email"></i> atendimento@sac.digital</a>
						                </div>
						            </div>
						            <div class="s-right hide_xs hide_xmd hide_tablet">
						                <h1>Seja um Representante</h1>
						                <p>Conheça os recursos da SAC DIGITAL e descubra como podemos levar sua empresa para um próximo nível.</p>
						                <a href="#" onclick="ga('sejaRepresentante.send', 'event', 'sejaRepresentante', 'click', 'Contato')">Seja um Representante</a>
						            </div>
												<div class="mas-info" style="float: left;margin-top: 30px;">
													<div class="r-phone-icon"><i class="fa fa-phone-square" style="color: white;"></i></div>
													<div class="r-suport">
					                    <p style="margin-bottom: 0px;"><b style="color:white;font-style:italic;">Precisa de mais informações?</b> <span style="text-align: right;color:white;">( 27 ) 99828 - 2828 <br>24hrs</span></p>
					                    <p style="color:white;">Fale com um de nossos consultores</p>
					                </div>
												</div>
											</div>
											<div class="col-md-6 formulario">
												<?php echo do_shortcode( '[contact-form-7 id="36" title="Contact form 1" html_id="mail"]' ); ?>
											</div>
						        </div>
						    </div>
							</div>
					</main><!-- #main -->
				</div><!-- #primary -->

			</div> <!--.row-->
  <?php get_footer(); ?>
